import urllib.request 
import json


def getPosition():
    
    response = urllib.request.urlopen('http://api.open-notify.org/iss-now.json')
    position = json.loads(response.read())['iss_position']
    print(position)
    return position
    

if __name__ == "__main__":
    getPosition()