
import os
import time
os.environ['PROJ_LIB'] = '/anaconda3/envs/isstracker/share/proj/'

from mpl_toolkits.basemap import Basemap
import numpy as np
import matplotlib.pyplot as plt
from find_iss import getPosition

def trackIss():
    # Setup of the map
    position = getPosition()
    # draw a black dot at the center.
    lat_0 = position['latitude'] 
    lon_0 = position['longitude']
    m = Basemap(projection='ortho',lat_0=lat_0,lon_0=lon_0)

    # fill background.
    m.drawmapboundary(fill_color='xkcd:sea blue')
    # draw coasts and fill continents.
    m.drawcoastlines(linewidth=0.5)
    m.fillcontinents(color='xkcd:leaf green',lake_color='aqua')
    # 20 degree graticule.
    m.drawparallels(np.arange(-80,81,20))
    m.drawmeridians(np.arange(-180,180,20))

    # Enable interactive mode
    plt.ion()
    plt.title('Position of the International Space Station')

    # Draw the plot
    xpt, ypt = m(lon_0, lat_0)
    plot_handle, = m.plot([xpt],[ypt],'wX', markeredgecolor='k')
    plt.draw()
    try:
        while(True):
            # Get position
            position = getPosition()
            lat_0 = position['latitude'] 
            lon_0 = position['longitude']
            xpt, ypt = m(lon_0, lat_0)

            # Plot point on the map
            plot_handle.set_ydata(ypt)
            plot_handle.set_xdata(xpt)

            # Update the plot
            plt.draw()
            plt.pause(3)
    except KeyboardInterrupt:
        print("Stopping tracking...")
        plt.close('all')


if __name__ == "__main__":
    try:
            trackIss()
    except KeyboardInterrupt:
        print("Shutting down...")
        exit
